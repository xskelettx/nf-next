#ifndef _LINUX_RTREE_H
#define _LINUX_RTREE_H


#include <linux/rcupdate.h>
#include <linux/spinlock.h>


typedef struct rtree_node* (*rtree_func_new)(struct rtree_node *const tmpl,
					     struct rtree_node *const left,
					     struct rtree_node *const right);

typedef void		   (*rtree_func_del)(struct rtree_node *const cur);

/*
 * cur < tmpl: -1
 * cur = tmpl:  0
 * cur > tmpl:  1
 */
typedef int		   (*rtree_func_cmp)(struct rtree_node *const tmpl,
					     struct rtree_node *const cur);

typedef void		   (*rtree_func_iter)(struct rtree_node *const cur);


struct rtree_node {
	size_t size;
	struct rtree_node __rcu *left;
	struct rtree_node __rcu *right;
	struct rcu_head rcu;
};


struct rtree {
	struct rtree_node __rcu *root;

	rtree_func_new new;
	rtree_func_del del;
	rtree_func_cmp cmp;

	spinlock_t lock;
};


void
rtree_init(struct rtree *const tree, rtree_func_new const new,
	   rtree_func_del const del, rtree_func_cmp const cmp);

void
rtree_connect(struct rtree_node *const cur, struct rtree_node *const left,
	      struct rtree_node *const right);

int
rtree_insert(struct rtree *const tree, struct rtree_node *const tmpl);

int
rtree_insert_dup(struct rtree *const tree, struct rtree_node *const tmpl);

struct rtree_node*
rtree_lookup(struct rtree *const tree, struct rtree_node *const needle);

void
rtree_foreach(struct rtree *const tree, rtree_func_iter const func);

int
rtree_delete(struct rtree *const tree, struct rtree_node *const tmpl);

void
rtree_destroy(struct rtree *const tree);

#endif /* _LINUX_RTREE_H */
