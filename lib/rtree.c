/*
 * This implements a frame to build performant self balancing trees out
 * of any data structure.
 *
 * Usage: 1) Define the wanted structure and embed `struct rtree_node` into it.
 *	  2) Implement the new, del and cmp callbacks for that data structure.
 *	      2.1) Those functions get pointers to the respective
 *		   `struct rtree_node` members.
 *	      2.2) Use `container_of` to get the actual address of the defined
 *		   structure.
 *	      2.3) Those functions shall use `ERR_PTR` to return failure state.
 *	      2.4) Deletion has to be done by `kfree_rcu`.
 *	  3) Pass address of `struct rtree_node` member to the `rtree_`
 *	     functions.
 */


#include <linux/rtree.h>
#include <linux/export.h>


#ifndef RTREE_WEIGHT
#   define RTREE_WEIGHT 4
#endif


#define deref_prot(ptr) \
	rcu_dereference_protected(ptr, lockdep_is_held(&tree->lock))


enum balance_mode {
	NONDESTRUCTIVE = 0,
	INPLACE	       = 1,
};


static size_t
node_size(struct rtree_node *const cur)
{
	return cur ? cur->size : 0;
}


static  struct rtree_node*
rotate_left_single(struct rtree *const tree, struct rtree_node *const left,
		   struct rtree_node *const right,
		   struct rtree_node *const tmpl)
{
	struct rtree_node *l, *new, *err;

	l = tree->new(tmpl, left, deref_prot(right->left));
	if (IS_ERR(l)) {
		err = l;
		goto error;
	}

	new = tree->new(right, l, deref_prot(right->right));
	if (IS_ERR(new)) {
		err = new;
		goto error_with_left;
	}

	tree->del(right);

	return new;

error_with_left:
	tree->del(l);
error:
	return err;
}


static struct rtree_node*
rotate_left_double(struct rtree *const tree, struct rtree_node *const left,
		   struct rtree_node *const right,
		   struct rtree_node *const tmpl)
{
	struct rtree_node *l, *r, *rl, *new, *err;

	rl = deref_prot(right->left);

	l = tree->new(tmpl, left, deref_prot(rl->left));
	if (IS_ERR(l)) {
		err = l;
		goto error;
	}

	r = tree->new(right, deref_prot(rl->right), deref_prot(right->right));
	if (IS_ERR(r)) {
		err = r;
		goto error_with_left;
	}

	new = tree->new(rl, l, r);
	if (IS_ERR(new)) {
		err = new;
		goto error_with_right;
	}

	tree->del(rl);
	tree->del(right);

	return new;

error_with_right:
	tree->del(r);
error_with_left:
	tree->del(l);
error:
	return err;
}


static struct rtree_node*
rotate_right_single(struct rtree *const tree, struct rtree_node *const left,
		    struct rtree_node *const right,
		    struct rtree_node *const tmpl)
{
	struct rtree_node *r, *new, *err;

	r = tree->new(tmpl, deref_prot(left->right), right);
	if (IS_ERR(r)) {
		err = r;
		goto error;
	}

	new = tree->new(left, deref_prot(left->left), r);
	if (IS_ERR(new)) {
		err = new;
		goto error_with_right;
	}

	tree->del(left);

	return new;

error_with_right:
	tree->del(r);
error:
	return err;
}


static struct rtree_node*
rotate_right_double(struct rtree *const tree, struct rtree_node *const left,
		    struct rtree_node *const right,
		    struct rtree_node *const tmpl)
{
	struct rtree_node *l, *r, *lr, *new, *err;

	lr = deref_prot(left->right);

	l = tree->new(left, deref_prot(left->left), deref_prot(lr->left));
	if (IS_ERR(l)) {
		err = l;
		goto error;
	}

	r = tree->new(tmpl, deref_prot(lr->right), right);
	if (IS_ERR(r)) {
		err = r;
		goto error_with_left;
	}

	new = tree->new(lr, l, r);
	if (IS_ERR(new)) {
		err = new;
		goto error_with_right;
	}

	tree->del(lr);
	tree->del(left);

	return new;

error_with_right:
	tree->del(r);
error_with_left:
	tree->del(l);
error:
	return err;
}


static struct rtree_node*
balance_left(struct rtree *const tree, struct rtree_node *const left,
	     struct rtree_node *const right, struct rtree_node *const tmpl)
{
	/* In case of failure, we pass the ERR_PTR through */

	if (node_size(deref_prot(right->left)) <
	    node_size(deref_prot(right->right)))
		return rotate_left_single(tree, left, right, tmpl);

	return rotate_left_double(tree, left, right, tmpl);
}


static struct rtree_node*
balance_right(struct rtree *const tree, struct rtree_node *const left,
	      struct rtree_node *const right, struct rtree_node *const tmpl)
{
	/* In case of failure, we pass the ERR_PTR through */

	if (node_size(deref_prot(left->right)) <
	    node_size(deref_prot(left->left)))
		return rotate_right_single(tree, left, right, tmpl);

	return rotate_right_double(tree, left, right, tmpl);
}


static struct rtree_node*
balance(struct rtree *const tree, struct rtree_node *const cur, const int cmp,
	struct rtree_node *const left, struct rtree_node *const right,
	enum balance_mode mode)
{
	size_t sz_left = node_size(left);
	size_t sz_right = node_size(right);
	struct rtree_node *new;

	if (sz_left + sz_right < 2)
		goto balanced;
	if (sz_right > RTREE_WEIGHT * sz_left)
		new = balance_left(tree, left, right, cur);
	else if (sz_left > RTREE_WEIGHT * sz_right)
		new = balance_right(tree, left, right, cur);
	else
		goto balanced;

	if (!IS_ERR(new))
		tree->del(cur);

	/* In case of failure, we pass the ERR_PTR through */
	return new;

balanced:
	if (mode == NONDESTRUCTIVE) {
		new = tree->new(cur, left, right);
		if (!IS_ERR(new))
			tree->del(cur);

		/* In case of failure, we pass the ERR_PTR through */
		return new;
	}

	if (cmp < 0) {
		BUG_ON(deref_prot(cur->right) != right);
		rcu_assign_pointer(cur->left, left);
	} else {
		BUG_ON(deref_prot(cur->left) != left);
		rcu_assign_pointer(cur->right, right);
	}

	/*
	* It is safe to set pointer and size in two nonatomic steps, because the
	* size is not used in read operations and there can only be one
	* modification at a time.
	*/
	cur->size = 1 + sz_left + sz_right;
	return cur;
}


static struct rtree_node*
insert(struct rtree *const tree, struct rtree_node *const cur,
       struct rtree_node *const tmpl)
{
	struct rtree_node *new;
	int cmp;

	if (!cur)
		return tmpl;

	cmp = tree->cmp(cur, tmpl);
	if (cmp < 0) {
		new = insert(tree, deref_prot(cur->left), tmpl);
		if (IS_ERR(new))
			return new;

		/* In case of failure, we pass the ERR_PTR through */
		return balance(tree, cur, cmp, new, deref_prot(cur->right),
			       INPLACE);
	}
	if (cmp > 0) {
		new = insert(tree, deref_prot(cur->right), tmpl);
		if (IS_ERR(new))
			return new;

		/* In case of failure, we pass the ERR_PTR through */
		return balance(tree, cur, cmp, deref_prot(cur->left), new,
			       INPLACE);
	}

	return cur;
}


static void
foreach(struct rtree_node *cur, rtree_func_iter const func)
{
	if (!cur)
		return;

	foreach(rcu_dereference(cur->left), func);
	func(cur);
	foreach(rcu_dereference(cur->right), func);
}


static struct rtree_node*
delete_min(struct rtree *const tree, struct rtree_node *const cur,
	   struct rtree_node **const min)
{
	struct rtree_node *l, *r, *new;

	l = deref_prot(cur->left);
	r = deref_prot(cur->right);

	if (!l) {
		*min = cur;
		return r;
	}

	new = delete_min(tree, l, min);
	if (IS_ERR(new))
		return new;

	/* In case of failure, we pass the ERR_PTR through */
	return balance(tree, cur, -1, new, r, NONDESTRUCTIVE);
}


static struct rtree_node*
delete(struct rtree *const tree, struct rtree_node *const cur,
       struct rtree_node *const tmpl)
{
	struct rtree_node *right;
	struct rtree_node *left;
	struct rtree_node *new;
	int cmp;

	if (!cur)
		return ERR_PTR(-ENOENT);

	left = deref_prot(cur->left);
	right = deref_prot(cur->right);

	cmp = tree->cmp(cur, tmpl);
	if (cmp < 0) {
		new = delete(tree, left, tmpl);
		if (IS_ERR(new))
			return new;

		/* In case of failure, we pass the ERR_PTR through */
		return balance(tree, cur, cmp, new, right, INPLACE);
	}
	if (cmp > 0) {
		new = delete(tree, right, tmpl);
		if (IS_ERR(new))
			return new;

		/* In case of failure, we pass the ERR_PTR through */
		return balance(tree, cur, cmp, left, new, INPLACE);
	}

	/* We found the node to delete */
	tree->del(cur);

	if (!left)
		return right;
	if (!right)
		return left;

	right = delete_min(tree, right, &new);
	if (IS_ERR(right))
		return right;


	/* In case of failure, we pass the ERR_PTR through */
	return balance(tree, new, 1, left, right, NONDESTRUCTIVE);
}


static void
destroy(struct rtree *const tree, struct rtree_node *const cur)
{
	if (!cur)
		return;

	/*
	 * We can use rcu_dereference here as we lock the tree only during the
	 * time we reset tree->root.
	 */
	destroy(tree, rcu_dereference(cur->left));
	destroy(tree, rcu_dereference(cur->right));

	tree->del(cur);
}


void
rtree_init(struct rtree *const tree, rtree_func_new const new,
	   rtree_func_del const del, rtree_func_cmp const cmp)
{
	rcu_assign_pointer(tree->root, NULL);
	spin_lock_init(&tree->lock);

	/*
	 * We want to offer regular struct initialization in favor of passing
	 * all the callbacks to this function.
	 */
	if (new)
		tree->new = new;
	if (del)
		tree->del = del;
	if (cmp)
		tree->cmp = cmp;

}
EXPORT_SYMBOL_GPL(rtree_init);


void
rtree_connect(struct rtree_node *const cur, struct rtree_node *const left,
	      struct rtree_node *const right)
{
	rcu_assign_pointer(cur->left, left);
	rcu_assign_pointer(cur->right, right);

	cur->size = 1 + node_size(left) + node_size(right);
}
EXPORT_SYMBOL_GPL(rtree_connect);


int
rtree_insert_dup(struct rtree *const tree, struct rtree_node *const tmpl)
{
	struct rtree_node *new;

	BUG_ON(!tree->new);

	new = tree->new(tmpl, NULL, NULL);
	if (IS_ERR(new))
		return PTR_ERR(new);

	return rtree_insert(tree, new);
}
EXPORT_SYMBOL_GPL(rtree_insert_dup);


int
rtree_insert(struct rtree *const tree, struct rtree_node *const tmpl)
{
	struct rtree_node *new;
	int ret = 0;

	BUG_ON(!tree->new);
	BUG_ON(!tree->cmp);
	BUG_ON(!tree->del);

	/*
	 * If rtree_insert is called from rtree_insert_dup, this will be
	 * executed twice unfortunately but that shouldn't be a big deal.
	 */
	rtree_connect(tmpl, NULL, NULL);

	spin_lock_bh(&tree->lock);

	new = insert(tree, deref_prot(tree->root), tmpl);
	if (IS_ERR(new)) {
		ret = PTR_ERR(new);
		goto out;
	}

	rcu_assign_pointer(tree->root, new);

out:
	spin_unlock_bh(&tree->lock);

	return ret;
}
EXPORT_SYMBOL_GPL(rtree_insert);


struct rtree_node*
rtree_lookup(struct rtree *const tree, struct rtree_node *const needle)
{
	struct rtree_node *cur;
	int cmp;

	cur = rcu_dereference(tree->root);
	while (cur && (cmp = tree->cmp(cur, needle)) != 0)
		cur = cmp < 0 ?
			rcu_dereference(cur->left) :
			rcu_dereference(cur->right);

	return cur;
}
EXPORT_SYMBOL_GPL(rtree_lookup);


void
rtree_foreach(struct rtree *const tree, rtree_func_iter const func)
{
	foreach(rcu_dereference(tree->root), func);
}
EXPORT_SYMBOL_GPL(rtree_foreach);


int
rtree_delete(struct rtree *const tree, struct rtree_node *const tmpl)
{
	struct rtree_node *new;
	int ret = 0;

	spin_lock_bh(&tree->lock);

	new = delete(tree, deref_prot(tree->root), tmpl);
	if (IS_ERR(new)) {
		ret = PTR_ERR(new);
		goto out;
	}

	rcu_assign_pointer(tree->root, new);

out:
	spin_unlock_bh(&tree->lock);

	return ret;
}
EXPORT_SYMBOL_GPL(rtree_delete);


void
rtree_destroy(struct rtree *const tree)
{
	struct rtree_node *old;

	spin_lock_bh(&tree->lock);

	old = deref_prot(tree->root);
	rcu_assign_pointer(tree->root, NULL);

	spin_unlock_bh(&tree->lock);

	destroy(tree, old);
}
EXPORT_SYMBOL_GPL(rtree_destroy);
