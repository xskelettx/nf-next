#include <linux/module.h>
#include <linux/rtree.h>
#include <linux/slab.h>
#include <linux/kthread.h>
#include <linux/delay.h>
#include <linux/vmalloc.h>


#define INFO   "rtree test info: "
#define FAILED "rtree test failed: "


#define MAX_ENTRIES	1000000


static int entries = 50000;
module_param(entries, int, 0);
MODULE_PARM_DESC(entries, "Number of entries to add (default: 50000)");


static int runs = 4;
module_param(runs, int, 0);
MODULE_PARM_DESC(runs, "Number of test runs per variant (default: 4)");


static int tcount = 10;
module_param(tcount, int, 0);
MODULE_PARM_DESC(tcount, "Number of threads to spawn (default: 10)");


struct test {
	struct rtree_node children;
	unsigned int	  payload;
};


/*
 * Just for readability reasons
 */
static struct test*
to_test(struct rtree_node *const node)
{
	if (!node || IS_ERR(node))
		return (void*)node;

	return container_of(node, struct test, children);
}


static struct rtree_node*
new(struct rtree_node *const _tmpl, struct rtree_node *const left,
    struct rtree_node *const right)
{
	unsigned int tmpl = to_test(_tmpl)->payload;
	struct test *new;

	new = kzalloc(sizeof(*new), GFP_KERNEL);
	if (!new)
		return ERR_PTR(-ENOMEM);

	new->payload = tmpl;

	rtree_connect(&new->children, left, right);

	return &new->children;
}


static int
cmp(struct rtree_node *const _tmpl, struct rtree_node *const _cur)
{
	unsigned int tmpl = to_test(_tmpl)->payload;
	unsigned int cur  = to_test(_cur)->payload;

	return cur < tmpl ? -1 : cur > tmpl ? 1 : 0;
}


static void
del(struct rtree_node *const _cur)
{
	struct test *cur = to_test(_cur);

	kfree_rcu(cur, children.rcu);
}


static struct rtree tree = {
	.cmp = cmp,
	.new = new,
	.del = del,
};


struct thread_data {
	int                id;
	struct task_struct *task;
};


/*
 * We cannot adapt the same logic as in the rhashtable tests here because we do
 * not necessarily get the identical node back, we inserted beforehand.
 * The rtree dublicates nodes during rotation to enable atomic insertions (and
 * frees unused ones).
 */
static int
_insert(struct rtree *const rt, unsigned int payload, int dub)
{
	struct test *obj;

	if (dub) {
		struct test cmp = {.payload = payload};

		return rtree_insert_dup(rt, &cmp.children);
	}

	obj = kzalloc(sizeof(*obj), GFP_KERNEL);
	if (!obj)
		return -ENOMEM;

	obj->payload = payload;

	return rtree_insert(rt, &obj->children);
}


static int
test_single_insert(struct rtree *const rt)
{
	unsigned int i;
	int err;

	for (i = 0; i < entries; ++i) {
		err = _insert(rt, i * 2, i & 1);
		if (err) {
			pr_err(FAILED "Could not insert new node\n");
			return err;
		}
	}

	return 0;
}


static int
test_single_lookup(struct rtree *const rt)
{
	struct test *obj;
	unsigned int i;

	for (i = 0; i < entries * 2; ++i) {
		struct test cmp = {.payload = i};
		int expected = !(i & 1);

		obj = to_test(rtree_lookup(rt, &cmp.children));

		if (expected && !obj) {
			pr_err(FAILED "Could not find payload %u\n", i);
			return -ENOENT;
		}
		if (!expected && obj) {
			pr_err(FAILED "Unexpected entry found for %u\n", i);
			return -EEXIST;
		}
		if (obj && obj->payload != cmp.payload) {
			pr_err(FAILED "Lookup value mismatch %u!=%u\n",
			       cmp.payload, obj->payload);
			return -EINVAL;
		}

		cond_resched_rcu();
	}

	return 0;
}


static s64
_test_single(struct rtree *rt)
{
	s64 start;
	s64 end;
	int err;

	start = ktime_get_ns();

	pr_info(INFO "starting insertion\n");
	err = test_single_insert(rt);
	if (err)
		return err;
	pr_info(INFO "finished insertion\n");

	pr_info(INFO "starting lookup\n");
	rcu_read_lock();
	err = test_single_lookup(rt);
	rcu_read_unlock();
	pr_info(INFO "finished lookup\n");

	if (err)
		return err;

	end = ktime_get_ns();
	return end - start;
}


static int
test_multiple_insert(struct thread_data *const tdata)
{
	int err, i;

	for (i = 0; i < entries; ++i) {
		err = _insert(&tree, tdata->id * entries + i, i & 1);
		if (err) {
			pr_err(FAILED "[%d]: Could not insert new node\n",
			       tdata->id);
			return err;
		}
	}

	return 0;
}


static int
test_multiple_lookup(struct thread_data *const tdata)
{
	struct test *obj;
	unsigned int i;

	for (i = 0; i < entries; ++i) {
		struct test cmp = {.payload = tdata->id * entries + i};

		obj = to_test(rtree_lookup(&tree, &cmp.children));

		if (!obj || IS_ERR(obj)) {
			pr_err(FAILED "Could not find payload %u\n",
			       cmp.payload);
			return -ENOENT;
		}
		if (obj && obj->payload != cmp.payload) {
			pr_err(FAILED "Lookup value mismatch %u!=%u\n",
			       obj->payload, cmp.payload);
			return -EINVAL;
		}

		cond_resched_rcu();
	}

	return 0;
}


static int
test_multiple_delete(struct thread_data *const tdata)
{
	int i, err, step;

	for (step = 0; step < entries; step += 10) {
		for (i = step; i < (step + 10) && i < entries; ++i) {
			struct test cmp = {.payload = tdata->id * entries + i};
			struct test *obj;

			err = rtree_delete(&tree, &cmp.children);
			if (err) {
				pr_err(FAILED "[%d]: Could not delete node\n",
				       tdata->id);
				return err;
			}

			cond_resched();

			rcu_read_lock();
			obj = to_test(rtree_lookup(&tree, &cmp.children));
			rcu_read_unlock();

			if (obj) {
				pr_err(FAILED "[%d]: Second lookup failed: expected -ENOENT\n",
				       tdata->id);
				return -EEXIST;
			}

			cond_resched();
		}
	}

	return 0;
}


static int
test_multiple_threadfunc(void *data) {
	struct thread_data *tdata = data;
	int err = 0;

	pr_info(INFO "[%d]: starting insertion\n", tdata->id);
	err = test_multiple_insert(tdata);
	if (err)
		goto out;
	pr_info(INFO "[%d]: finished insertion\n", tdata->id);

	pr_info(INFO "[%d]: starting lookup\n", tdata->id);
	rcu_read_lock();
	err = test_multiple_lookup(tdata);
	rcu_read_unlock();
	if (err)
		goto out;
	pr_info(INFO "[%d]: finished lookup\n", tdata->id);

	pr_info(INFO "[%d]: starting deletion and second lookup\n", tdata->id);
	err = test_multiple_delete(tdata);
	pr_info(INFO "[%d]: finished deletion and second lookup\n", tdata->id);

out:
	while (!kthread_should_stop())
		msleep(20);

	return err;
}


static int
test_multiple(void) {
	struct thread_data *tdata = NULL;
	int threads_started = 0;
	int threads_failed = 0;
	int err;
	int i;

	if (!tcount)
		return 0;

	pr_info(INFO "Testing concurrent rtree accesses: %d threads\n", tcount);

	tdata = vzalloc(tcount * sizeof(*tdata));
	if (!tdata) {
		pr_err(FAILED "Could not allocate thread data\n");
		return -ENOMEM;
	}

	rtree_init(&tree, NULL, NULL, NULL);
	for (i = 0; i < tcount; ++i) {
		tdata[i].id = i;
		tdata[i].task = kthread_run(test_multiple_threadfunc, &tdata[i],
					    "rtree-thread[%d]", i);

		if (IS_ERR(tdata[i].task))
			pr_err(FAILED "kthread_run failed for thread %d\n", i);
		else
			++threads_started;
	}

	for (i = 0; i < tcount; ++i) {
		if (IS_ERR(tdata[i].task))
			continue;

		err = kthread_stop(tdata[i].task);
		if (!err)
			continue;

		pr_err(FAILED "Test failed: thread %d returned: %d", i, err);
		++threads_failed;
	}

	pr_info(INFO "threads started: %d, treads failed: %d\n",
		threads_started, threads_failed);

	rtree_destroy(&tree);
	vfree(tdata);

	return -threads_failed ? : 0;
}


static int
test_single(struct rtree *const rt) {
	u64 time_total = 0;
	int i;

	for (i = 0; i < runs; ++i) {
		s64 time;

		pr_info(INFO "Test %03d\n", i);

		rtree_init(rt, NULL, NULL, NULL);
		time = _test_single(rt);
		rtree_destroy(rt);

		if (time < 0)
			return time;

		time_total += time;
	}

	return time_total / 1000;
}


static int __init
test_rtree_init(void)
{
	int threads_failed;
	u64 time_total;

	entries = min(entries, MAX_ENTRIES);

	BUG_ON(entries * tcount >= UINT_MAX - 10);

	time_total = test_single(&tree);
	if (time_total < 0)
		return time_total;

	pr_info(INFO "Average test time (micro seconds): %llu\n",
		time_total / runs);

	threads_failed = test_multiple();
	if (threads_failed < 0)
		return threads_failed;

	return 0;
}


static void __exit
test_rtree_exit(void)
{
}


module_init(test_rtree_init);
module_exit(test_rtree_exit);


MODULE_LICENSE("GPL v2");
